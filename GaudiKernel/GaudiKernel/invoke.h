#ifndef GAUDIKERNEL_INVOKE_H
#define GAUDIKERNEL_INVOKE_H
#include <functional>

#warning "Obsolete header, please use std::invoke"

namespace Gaudi {
  using std::invoke;
}
#endif
