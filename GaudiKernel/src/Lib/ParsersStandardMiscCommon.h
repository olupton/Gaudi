#ifndef PARSERS_STANDARD_MISC_COMMON_H
#define PARSERS_STANDARD_MISC_COMMON_H 1
// ============================================================================
// Include files
// ============================================================================
#include <Gaudi/Parsers/CommonParsers.h>
#include <Gaudi/Parsers/Factory.h>
// ============================================================================
// STD & STL
// ============================================================================
#include <map>
#include <set>
#include <string>
#include <vector>
// ============================================================================
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/StringKey.h"
// ============================================================================
#endif /* PARSERS_STANDARD_MISC_COMMON_H */
